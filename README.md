# README #

### What is this repository for? ###

This was an end of class project for a high performance computing class I took.

I (Dwyer) programmed our implementation of Cannon's algorithm it was a hybrid multi-processed/multi-threaded version
because using multiple threads is more efficient than a serial mat-mult kernel. There are more details in the final report.
Not all the code is mine mostly just the implementation of Cannon's algorithm & the matrix inversion code which is unfinished.

There aren't detailed instructions on how to use this right now it is more meant as a display of some of my work. I may get
around to making instructions at some point though.
