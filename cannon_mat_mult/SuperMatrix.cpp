#include "SuperMatrix.h"

Matrix& SuperMatrix1D::operator[](int row)
{
    return mat_vec[row];
}
Matrix* SuperMatrix2D::operator[](int row)
{
    return &mat_vec[row*dim_sz2];
}
Matrix& SuperMatrix2D::mat(int row, int col)
{
    return mat_vec[row*dim_sz2 + col];
}

void SuperMatrix2D::loadInMatrix(Matrix original_mat, int super_mat_dim)
{
    dim_sz1 = super_mat_dim;
    dim_sz2 = super_mat_dim;

    mat_vec.resize(dim_sz1*dim_sz2);

    size_t sub_mat_col_dim = original_mat.n_cols() / dim_sz1;
    size_t sub_mat_row_dim = original_mat.n_rows() / dim_sz2;
    int col_remainder = original_mat.n_cols() % dim_sz1;
    int row_remainder = original_mat.n_rows() % dim_sz2;

    // whenever a submatrix's i or j is < remainder
    // then the i or j dim size is +1 and the offset is (i or j)*(sub_mat_dim+1)
    // but whenever a submatrix i or j is >= remainder
    // the then i or j dim size is +0 and the offset is (i or j)*sub_mat_dim + remainder
    for (int i = 0; i < row_remainder; i++)
    {
        // this makes the 3x3 submat in our example
        // both i and j < remainder so both i and j dims get +1
        for (int j = 0; j < col_remainder; j++)
        {
            //int i_offset = i*(sub_mat_dim+1);
            //int j_offset = j*(sub_mat_dim+1);

            mat(i,j) = original_mat.getSubMatrix(i*(sub_mat_row_dim+1),j*(sub_mat_col_dim+1),sub_mat_row_dim+1,sub_mat_col_dim+1);
        }
        // only i < remainder so only i dim is +1
        // since only i dim is +1 j offset is the larger one (+remainder)
        for (int j = col_remainder; j < dim_sz2; j++)
        {
            //int i_offset = i*(sub_mat_dim+1);
            //int j_offset = j*sub_mat_dim + remainder;

            mat(i,j) = original_mat.getSubMatrix(i*(sub_mat_row_dim+1), j*sub_mat_col_dim + col_remainder, sub_mat_row_dim+1, sub_mat_col_dim);
            //mat(i,j).print();
        }
    }
    for (int i = row_remainder; i < dim_sz1; i++)
    {
        // only j dim is < remainder so only j dim gets +1
        // since j dim gets +1 i offset gets +remainder
        for (int j = 0; j < col_remainder; j++)
        {
            //int i_offset = i*sub_mat_dim + remainder;
            //int j_offset = j*(sub_mat_dim+1);

            mat(i,j) = original_mat.getSubMatrix(i*sub_mat_row_dim + row_remainder, j*(sub_mat_col_dim+1), sub_mat_row_dim, sub_mat_col_dim+1);
            //mat(i,j).print();
        }
        for (int j = col_remainder; j < dim_sz2; j++)
        {
            //int i_offset = i*sub_mat_dim + remainder;
            //int j_offset = j*sub_mat_dim + remainder;

            mat(i,j) = original_mat.getSubMatrix(i*sub_mat_row_dim + row_remainder, j*sub_mat_col_dim + col_remainder, sub_mat_row_dim, sub_mat_col_dim);
            //mat(i,j).print();
        }
    }
}

void SuperMatrix1D::loadInMatrixCols(Matrix original_mat, int n_cols)
{
    mat_vec.resize(n_cols);

    size_t sub_mat_dim = original_mat.n_cols() / mat_vec.size();
    int remainder = original_mat.n_cols() % mat_vec.size();

    for (int i = 0; i < remainder; i++)
    {
        mat_vec[i] = original_mat.getSubMatrix(0, i*(sub_mat_dim+1), original_mat.n_rows(), sub_mat_dim+1);
    }
    for (int i = remainder; i < mat_vec.size(); i++)
    {
        mat_vec[i] = original_mat.getSubMatrix(0, i*sub_mat_dim + remainder, original_mat.n_rows(), sub_mat_dim);
    }
}

void SuperMatrix1D::loadInMatrixRows(Matrix original_mat, int n_rows)
{
    mat_vec.resize(n_rows);

    size_t sub_mat_dim = original_mat.n_rows() / mat_vec.size();
    int remainder = original_mat.n_rows() % mat_vec.size();

    for (int i = 0; i < remainder; i++)
    {
        mat_vec[i] = original_mat.getSubMatrix(i*(sub_mat_dim+1), 0, sub_mat_dim+1, original_mat.n_cols());
    }
    for (int i = remainder; i < mat_vec.size(); i++)
    {
        mat_vec[i] = original_mat.getSubMatrix(i*sub_mat_dim + remainder, 0, sub_mat_dim, original_mat.n_cols());
    }
}

// mat_A is the matrix on the left in the cannon algorithm
void SuperMatrix2D::alignAsSuperMatA()
{
    Matrix mat_buffer[dim_sz2];

    for (int i = 0; i < dim_sz1; i++)
    {
        for (int j = 0; j < dim_sz2; j++)
        {
            int new_j = (i+j)%dim_sz2;
            mat_buffer[j] = mat(i,j);
            mat(i,j) = new_j < j ? mat_buffer[new_j] : mat(i,new_j);
        }
    }
}

// mat_B is the matrix on the right in the cannon algorithm
void SuperMatrix2D::alignAsSuperMatB()
{
    Matrix mat_buffer[dim_sz1];

    for (int j = 0; j < dim_sz2; j++)
    {
        for (int i = 0; i < dim_sz1; i++)
        {
            int new_i = (i+j)%dim_sz1;
            mat_buffer[i] = mat(i,j);
            mat(i,j) = new_i < i ? mat_buffer[new_i] : mat(new_i,j);
        }
    }
}

size_t SuperMatrix2D::requiredScatterBufferSz()
{
    if (mat_vec.size() > 0)
    {
        size_t sub_mat_size = mat(0,0).n_rows()*mat(0,0).n_cols()*sizeof(double) + sizeof(Matrix);
        return sub_mat_size*dim_sz1*dim_sz2;
    }
    else return 0;
}

void SuperMatrix2D::dumpToScatterBuffer(void* buffer, size_t& sub_mat_size)
{

    // should be max byte size of sub_mats
    sub_mat_size = mat(0,0).n_rows()*mat(0,0).n_cols()*sizeof(double) + sizeof(Matrix);

    for (int i = 0; i < dim_sz1; i++)
    {
        for (int j = 0; j < dim_sz2; j++)
        {
            mat(i,j).dumpToBuffer(buffer);
            buffer += sub_mat_size;
        }
    }
}

void SuperMatrix2D::copyFromGatherBuffer(void* buffer)
{
    // should be max byte size of sub_mats
    size_t sub_mat_size = mat(0,0).n_rows()*mat(0,0).n_cols()*sizeof(double) + sizeof(Matrix);

    for (int i = 0; i < dim_sz1; i++)
    {
        for (int j = 0; j < dim_sz2; j++)
        {
            mat(i,j).copyFromBuffer(buffer);
            buffer += sub_mat_size;
        }
    }
}
