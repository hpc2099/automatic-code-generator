#include <cstdio>
#include <cmath>
#include <memory>
#include <random>
#include <iostream>
#include <fstream>
#include <ctime>
#include <cstring>
#include <string>
#include <vector>
#include <sstream>

#include "ThreadPool.h"
#include "Matrix.h"
#include "SuperMatrix.h"

#define USE_MPI
//#define BETTER_SEND_RECV
#define MY_ALIGNMENT_FUNC

#ifdef USE_MPI
#include <mpi.h>
#endif

mt19937 rand_generator;

using namespace std;

//#define CONCAT(a, b) a##b
//#define MACRO_CONCAT(a, b) CONCAT(a, b)
//#define UNIQUENAME(prefix) MACRO_CONCAT(prefix, __LINE__)

//#define MAKE_STATIC_MATRIX(mat_name,rows,cols) char CONCAT(mat_name,buff)[sizeof(Matrix)+(rows*cols)*sizeof(double)]; Matrix& mat_name = *((Matrix*)CONCAT(mat_name,buff)); mat_name = Matrix(rows,cols,(double*)(CONCAT(mat_name,buff)+sizeof(Matrix)));

Matrix parallel_dotSum(Matrix left_mat, Matrix right_mat, ThreadPool& pool, Matrix result_mat = Matrix())
{
    int super_mat_dim = (int)(sqrt(pool.size()) + 0.999999999);

    if (result_mat.n_cols() == 0) // if not valid
    {
        result_mat = move(Matrix(left_mat.n_rows(),right_mat.n_cols()));
        result_mat.zero_out();
    }

    SuperMatrix2D result_super_mat;
    SuperMatrix1D left_row_super_mat;
    SuperMatrix1D right_col_super_mat;

    result_super_mat.loadInMatrix(result_mat, super_mat_dim);
    left_row_super_mat.loadInMatrixRows(left_mat, super_mat_dim);
    right_col_super_mat.loadInMatrixCols(right_mat, super_mat_dim);

    auto meta_dot_thread_func = [](Matrix mat_to_comp, Matrix left_row, Matrix right_col)
    {
        left_row.dotSum(right_col,mat_to_comp);
    };

    for (int i = 0; i < super_mat_dim; i++)
    {
        for (int j = 0; j < super_mat_dim; j++)
        {
            //meta_dot_thread_func(result_super_mat[i][j],left_row_super_mat[i],right_col_super_mat[j]);
            pool.enqueue(meta_dot_thread_func, result_super_mat[i][j], left_row_super_mat[i], right_col_super_mat[j]);
        }
    }

    // super mats not needed all you need to do for each submat C is a serial dot of the
    // row of A that C is in with the column of B that C is in
    // also might be better to use omp parallel for when looping over submats

    return result_mat;
}

Matrix parallel_dot(Matrix left_mat, Matrix right_mat, ThreadPool& pool, Matrix result_mat = Matrix())
{
    if (result_mat.n_cols() == 0)
    {
        return parallel_dotSum(left_mat,right_mat,pool);
    }
    else
    {
        result_mat.zero_out();
        return parallel_dotSum(left_mat,right_mat,pool,result_mat);
    }
}

void single_process_test()
{
    int dim = 10;

    Matrix A, B, result_mat;

    A = Matrix(dim,dim);
    B = Matrix(dim,dim);
    result_mat = Matrix(dim,dim);

    int n = 0;

    for (int i = 0; i < dim; i++)
    {
        for (int j = 0; j < dim; j++)
        {
            A[i][j] = n;
            B[i][j] = n + 5;
            n++;
        }
    }

    int num_cpus = thread::hardware_concurrency();

    ThreadPool pool(num_cpus);

    time_t start = time(0);
    parallel_dot(A, B, pool, result_mat);

    pool.block_til_work_finished();
    time_t duration = time(0) - start;

    printf("mat A:\n\n");
    A.print();
    printf("mat B:\n\n");
    B.print();
    printf("result_mat:\n\n");
    result_mat.print();

    cout << "duration: " << duration << endl;
}

/*int main()
{
    single_process_test();
    return 0;
}/**/

ofstream debug_f;

// only works for square matrices right now
int main(int argc, char** argv)
{
    rand_generator.seed(time(0));
    double rand_min = 0;
    double rand_max = 500;

    int rows = 500;
    int cols = 500;

    int num_threads = 0;
    if (argc > 2)
    {
        stringstream ss(argv[2]);
        ss >> num_threads;
    }
    else if (argc > 1)
    {
        stringstream ss(argv[1]);
        ss >> rows;
        cols = rows;
    }

    if (num_threads == 0)
    {
        num_threads = thread::hardware_concurrency();
        if (num_threads == 0)
        {
            cout << "couldn't detect number of cores defaulting to 8 threads" << endl;
            num_threads = 8;
        }
    }

    int size = 4, rank = 0; // values for debugging
    #ifdef USE_MPI
    MPI_Init(NULL,NULL);

    MPI_Comm_size(MPI_COMM_WORLD,&size);
    MPI_Comm_rank(MPI_COMM_WORLD,&rank);
    #endif

    #ifdef CHECK_POINT
    stringstream ss;
    ss << rank;
    debug_f.open("progress_report" + ss.str() + ".csv");
    #endif

    size_t super_mat_dim = sqrt(size);

    if (super_mat_dim*super_mat_dim != size)
    {
        cerr << "number of processes must be an even square i.e. 4, 16, 25..." << endl;
        return -1;
    }

    Matrix A, B, result_mat;
    SuperMatrix2D super_mat_A, super_mat_B;
    SuperMatrix2D result_super_mat;

    if (rank == 0)
    {
        A = Matrix(rows,cols);
        B = Matrix(cols,rows);
        result_mat = Matrix(rows,rows);

        super_mat_A.loadInMatrix(A,super_mat_dim);
        super_mat_B.loadInMatrix(B,super_mat_dim);
        result_super_mat.loadInMatrix(result_mat, super_mat_dim);

        #ifdef MY_ALIGNMENT_FUNC
        super_mat_A.alignAsSuperMatA();
        super_mat_B.alignAsSuperMatB();
        #endif

        for (int i = 0; i < rows; i++)
        {
            for (int j = 0; j < cols; j++)
            {
                A[i][j] = rand_min + (rand_generator()/(double)rand_generator.max()) * (rand_max - rand_min);
                B[j][i] = rand_min + (rand_generator()/(double)rand_generator.max()) * (rand_max - rand_min);
            }
        }
    }

    size_t sub_mat_sz=0;

    int periods[]={1,1}; //both vertical and horizontal movement;
    int dims[]={super_mat_dim,super_mat_dim};
    int coords[2]; // 2 Dimension topology so 2 coordinates
    int neighbor_coors[2];

    size_t scatterBuffSz = super_mat_A.requiredScatterBufferSz();
    char scatterBuffer[scatterBuffSz];

    if (rank==0) super_mat_A.dumpToScatterBuffer(scatterBuffer, sub_mat_sz);

    #ifdef USE_MPI
    MPI_Bcast(&sub_mat_sz,1,MPI_INT,0,MPI_COMM_WORLD);
    #endif // USE_MPI

    bool using_buff1 = true;

    char sub_mat_a_buff1[sub_mat_sz];
    char sub_mat_a_buff2[sub_mat_sz];
    char sub_mat_b_buff1[sub_mat_sz];
    char sub_mat_b_buff2[sub_mat_sz];
    char sub_result_mat_buff[sub_mat_sz];

    void* alt_buff_a = sub_mat_a_buff2;
    void* alt_buff_b = sub_mat_b_buff2;

    Matrix* sub_result_mat = ((Matrix*)sub_result_mat_buff);
    Matrix* sub_mat_a = ((Matrix*)sub_mat_a_buff1);
    Matrix* sub_mat_b = ((Matrix*)sub_mat_b_buff1);

    memset(sub_result_mat_buff,0,sub_mat_sz);

    time_t start = time(0);

    #ifdef USE_MPI
    MPI_Comm cart_comm;
    MPI_Cart_create(MPI_COMM_WORLD,2,dims,periods,1,&cart_comm);
    MPI_Comm_rank(cart_comm,&rank);
    MPI_Cart_coords(cart_comm,rank,2,coords);
    MPI_Scatter(scatterBuffer,sub_mat_sz,MPI_CHAR,sub_mat_a,sub_mat_sz,MPI_CHAR,0,cart_comm);
    #endif

    int right=rank, left=rank, down=rank, up=rank; // neighbor ranks

    if (rank==0) super_mat_B.dumpToScatterBuffer(scatterBuffer, sub_mat_sz);
    #ifdef USE_MPI
    MPI_Scatter(scatterBuffer,sub_mat_sz,MPI_CHAR,sub_mat_b,sub_mat_sz,MPI_CHAR,0,cart_comm);

    #ifndef MY_ALIGNMENT_FUNC
    MPI_Cart_shift(cart_comm, 1, coords[0], &left,&right);
    MPI_Cart_shift(cart_comm, 0, coords[1], &up,&down);
    #endif

    #ifndef MY_ALIGNMENT_FUNC
    #ifndef BETTER_SEND_RECV
    MPI_Sendrecv_replace(sub_mat_a,sub_mat_sz,MPI_CHAR,left,11,right,11,cart_comm,MPI_STATUS_IGNORE);
    MPI_Sendrecv_replace(sub_mat_b,sub_mat_sz,MPI_CHAR,up,11,down,11,cart_comm,MPI_STATUS_IGNORE);
    #else
    MPI_Sendrecv(sub_mat_a, sub_mat_sz, MPI_CHAR, left, 11, alt_buff_a, sub_mat_sz, MPI_CHAR, right, 11, cart_comm, MPI_STATUS_IGNORE);
    MPI_Sendrecv(sub_mat_b, sub_mat_sz, MPI_CHAR, up, 11, alt_buff_b, sub_mat_sz, MPI_CHAR, down, 11, cart_comm, MPI_STATUS_IGNORE);

    if (using_buff1)
    {
        sub_mat_a = ((Matrix*)sub_mat_a_buff2);
        sub_mat_b = ((Matrix*)sub_mat_b_buff2);
        alt_buff_a = sub_mat_a_buff1;
        alt_buff_b = sub_mat_b_buff1;
    }
    else
    {
        sub_mat_a = ((Matrix*)sub_mat_a_buff1);
        sub_mat_b = ((Matrix*)sub_mat_b_buff1);
        alt_buff_a = sub_mat_a_buff2;
        alt_buff_b = sub_mat_b_buff2;
    }
    using_buff1 = !using_buff1;
    #endif
    #endif

    ThreadPool pool(num_threads);

    // will have same dimensions
    *sub_result_mat = *sub_mat_a;

    // make the submats valid
    sub_result_mat->moveDataPtrBehindMat();
    sub_mat_a->moveDataPtrBehindMat();
    sub_mat_b->moveDataPtrBehindMat();

    for(int i=0;i<super_mat_dim;i++)
    {
        //time_t start = time(0);

        // TODO: might want to give this a spin lock
        #ifdef BETTER_SEND_RECV
        pool.block_til_work_finished();
        #endif
        parallel_dotSum(*sub_mat_a, *sub_mat_b, pool, *sub_result_mat);
        #ifndef BETTER_SEND_RECV
        pool.block_til_work_finished();
        #endif
        //time_t duration = time(0) - start;

        MPI_Cart_shift(cart_comm, 1, 1, &left,&right);
        MPI_Cart_shift(cart_comm, 0, 1, &up,&down);

        #ifdef BETTER_SEND_RECV
        MPI_Sendrecv(sub_mat_a, sub_mat_sz, MPI_CHAR, left, 11, alt_buff_a, sub_mat_sz, MPI_CHAR, right, 11, cart_comm, MPI_STATUS_IGNORE);
        MPI_Sendrecv(sub_mat_b, sub_mat_sz, MPI_CHAR, up, 11, alt_buff_b, sub_mat_sz, MPI_CHAR, down, 11, cart_comm, MPI_STATUS_IGNORE);

        if (using_buff1)
        {
            sub_mat_a = ((Matrix*)sub_mat_a_buff2);
            sub_mat_b = ((Matrix*)sub_mat_b_buff2);
            alt_buff_a = sub_mat_a_buff1;
            alt_buff_b = sub_mat_b_buff1;
        }
        else
        {
            sub_mat_a = ((Matrix*)sub_mat_a_buff1);
            sub_mat_b = ((Matrix*)sub_mat_b_buff1);
            alt_buff_a = sub_mat_a_buff2;
            alt_buff_b = sub_mat_b_buff2;
        }
        using_buff1 = !using_buff1;/**/

        #else
        MPI_Sendrecv_replace(sub_mat_a,sub_mat_sz,MPI_CHAR,left,11,right,11,cart_comm,MPI_STATUS_IGNORE);
        MPI_Sendrecv_replace(sub_mat_b,sub_mat_sz,MPI_CHAR,up,11,down,11,cart_comm,MPI_STATUS_IGNORE);
        #endif
        sub_mat_a->moveDataPtrBehindMat();
        sub_mat_b->moveDataPtrBehindMat();
    }

    MPI_Gather(sub_result_mat_buff, sub_mat_sz, MPI_CHAR, scatterBuffer, sub_mat_sz, MPI_CHAR, 0, cart_comm);

    if (rank == 0)
    {
        result_super_mat.copyFromGatherBuffer(scatterBuffer);

        //printf("mat A:\n\n");
        //A.print();
        //printf("mat B:\n\n");
        //B.print();
        //printf("result_mat:\n\n");
        //result_mat.print();

        time_t duration = time(0) - start;

        cout << "done" << endl;
        cout << "execution time (sec): " << duration << endl;

        time_t rawtime;
        tm* timeinfo;
        time (&rawtime);
        timeinfo = localtime (&rawtime);

        ofstream bench_mark_csv("bench_marks.csv",ios::app);
        bench_mark_csv << rows << ", " << cols << ", " << duration << " # " << asctime(timeinfo) << endl;

        ofstream A_csv("A_mat.csv");
        ofstream B_csv("B_mat.csv");
        ofstream Result_csv("Result_mat.csv");

        A.fprint(A_csv, 60);
        B.fprint(B_csv, 60);
        result_mat.fprint(Result_csv, 60);
    }

    MPI_Finalize();
    #endif
    return 0;
}/**/
