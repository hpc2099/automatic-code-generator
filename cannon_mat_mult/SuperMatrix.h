#ifndef SUPERMATRIX_H
#define SUPERMATRIX_H

#include <vector>
#include "Matrix.h"

class SuperMatrix1D
{
    public:
        SuperMatrix1D() = default;

        Matrix& operator[](int row);

        int getSize()
        {
            return mat_vec.size();
        }

        void loadInMatrixCols(Matrix original_mat, int n_cols);
        void loadInMatrixRows(Matrix original_mat, int n_rows);
    private:
        vector<Matrix> mat_vec;
};

class SuperMatrix2D
{
    public:
        SuperMatrix2D() = default;

        Matrix* operator[](int row);

        void getDims(int& dim_sz1, int& dim_sz2)
        {
            dim_sz1 = this->dim_sz1;
            dim_sz2 = this->dim_sz2;
        }

        void loadInMatrix(Matrix original_mat, int super_mat_dim);

        void alignAsSuperMatA();
        void alignAsSuperMatB();

        size_t requiredScatterBufferSz();
        void dumpToScatterBuffer(void* buffer, size_t& sub_mat_size);
        void copyFromGatherBuffer(void* buffer);

    private:
        Matrix& mat(int row, int col);

        int dim_sz1, dim_sz2;
        vector<Matrix> mat_vec;
};

#endif // SUPERMATRIX_H
