#ifndef MATRIX_H
#define MATRIX_H

#include <iostream>
#include <stdexcept>
#include <iomanip>
#include <cstring>
#include <cstdio>
#include <string>
#include <sstream>
#include <fstream>

using namespace std;

//#define CHECK_POINT debug_f << __FILE__ << ": " << __LINE__ << endl; debug_f.flush();

extern ofstream debug_f;

class Matrix
{
    private:
    double* data_ptr;
    size_t _n_rows, _n_cols;
    size_t row_spacing;
    bool owns_data;

    void copyFromOther(const Matrix& other)
    {
        data_ptr = other.data_ptr;
        _n_rows = other.n_rows();
        _n_cols = other.n_cols();
        row_spacing = other.row_spacing;
    }

    public:

    inline size_t n_rows() const { return _n_rows; }
    inline size_t n_cols() const { return _n_cols; }

    constexpr Matrix() :
        data_ptr(nullptr),
        _n_rows(0),
        _n_cols(0),
        row_spacing(0),
        owns_data(false)
    { }

    Matrix(size_t n_rows, size_t n_cols) :
        data_ptr(new double[n_rows*n_cols]),
        _n_rows(n_rows),
        _n_cols(n_cols),
        row_spacing(n_cols),
        owns_data(true)
    { }

    Matrix(size_t n_rows, size_t n_cols, double* data_ptr) :
        data_ptr(data_ptr),
        _n_rows(n_rows),
        _n_cols(n_cols),
        row_spacing(n_cols),
        owns_data(false)
    { }

     Matrix(Matrix&& other)
    {
        copyFromOther(other);
        owns_data = other.owns_data;
        other.owns_data = false;
    }

    Matrix(const Matrix& other)
    {
        copyFromOther(other);
        owns_data = false;
    }

    ~Matrix()
    {
        if (owns_data)
        {
            delete[] data_ptr;
        }
    }

    inline double& data(size_t i, size_t j)
    {
        return *(data_ptr + row_spacing * i + j);
    }

    inline double* operator[](size_t row)
    {
        return data_ptr + row_spacing * row;
    }

    Matrix& operator=(const Matrix& other)
    {
        copyFromOther(other);
        owns_data = false;
        return *this;
    }

    Matrix& operator=(Matrix&& other)
    {
        copyFromOther(other);
        owns_data = other.owns_data;
        other.owns_data = false;
        return *this;
    }

    void operator+=(Matrix& other_mat)
    {
        for (int i = 0; i < n_rows(); i++)
        {
            for (int j = 0; j < n_cols(); j++)
            {
                data(i,j) += other_mat[i][j];
            }
        }
    }

    bool operator==(Matrix& other)
    {
        if (other.n_cols() == n_cols() && other.n_rows() == n_rows())
        {
            for (int i = 0; i < n_rows(); i++)
            {
                for (int j = 0; j < n_cols(); j++)
                {
                    if (data(i,j) != other[i][j])
                    {
                        return false;
                    }
                }
            }
            return true;
        }
        else return false;
    }

    void zero_out()
    {
        if (n_cols() == row_spacing)
        {
            memset(data_ptr,0,sizeof(double)*n_cols()*n_rows());
        }
        else
        {
            for (int i = 0; i < n_rows(); i++)
            {
                memset(&data(i,0),0,sizeof(double)*n_cols());
            }
        }
    }

    void copyFromBuffer(void* buffer)
    {
        ((Matrix*)buffer)->moveDataPtrBehindMat();

        Matrix other_mat = *(Matrix*)buffer;

        if (n_rows() != other_mat.n_rows() || n_cols() != other_mat.n_cols())
        {
            stringstream ss;
            ss << "buffer contains a matrix of incompatible dimensions,\nrequired rows = " << n_rows() << ", required cols = " << n_cols();
            ss << ",\ncolumns found = " << other_mat.n_cols() << ", rows found = " << other_mat.n_rows() << endl;
            throw invalid_argument(ss.str());
        }

        for (int i = 0; i < n_rows(); i++)
        {
            for (int j = 0; j < n_cols(); j++)
            {
                data(i,j) = other_mat[i][j];
            }
        }
    }

    size_t requiredDumpBuffSz()
    {
        return sizeof(Matrix) + sizeof(double)*n_cols()*n_rows();;
    }

    size_t dumpToBuffer(void* buffer)
    {
        Matrix* mat_copy = (Matrix*)buffer;
        *mat_copy = Matrix(*this);
        mat_copy->row_spacing = n_cols();
        mat_copy->owns_data = false;

        auto double_buff = (double (*)[n_cols()])(buffer + sizeof(Matrix));

        for (int i = 0; i < n_rows(); i++)
        {
            for (int j = 0; j < n_cols(); j++)
            {
                double_buff[i][j] = data(i,j);
            }
        }

        return sizeof(Matrix) + sizeof(double)*n_cols()*n_rows();
    }

    // use this after you get a matrix that has been dumped to buffer
    void moveDataPtrBehindMat()
    {
        data_ptr = (double*)(((char*)this) + sizeof(Matrix));
    }

    double* getDataBuffer()
    {
        return data_ptr;
    }

    void fprint(std::ostream& stream, int precision = 6)
    {
        //auto data_wrapper = (double (*)[row_spacing])data_ptr;

        stream << "#printing " << n_rows() << "X" << n_cols() << " matrix:\n\n";

        for (int i = 0; i < n_rows(); i++)
        {
            stream << setw(8) << setprecision(precision) << data(i,0);
            for (int j = 1; j < n_cols(); j++)
            {
                stream << ", " << setw(8) << setprecision(precision) << data(i,j);
            }
            stream << endl;
        }
        stream << endl;
    }

    void print()
    {
        fprint(std::cout);
    }

    Matrix getSubMatrix(size_t row_offset, size_t col_offset, size_t n_rows1, size_t n_cols1)
    {
        double* new_data_ptr = data_ptr + row_offset * row_spacing + col_offset;
        Matrix subMatrix(n_rows1, n_cols1, new_data_ptr);

        subMatrix.row_spacing = row_spacing;

        return subMatrix;
    }

    Matrix getQuandrant(int q_num)
    {
        switch (q_num)
        {
        case 1:
            return getSubMatrix(0,0,n_rows()/2,n_cols()/2);
        case 2:
            return getSubMatrix(0,n_cols()/2,n_rows()/2,(n_cols()+1)/2);
        case 3:
            return getSubMatrix(n_rows()/2,0,(n_rows()+1)/2,n_cols()/2);
        case 4:
            return getSubMatrix(n_rows()/2,n_cols()/2,(n_rows()+1)/2,(n_cols()+1)/2);
        default:
            throw invalid_argument("q_num must be from 1-4");
        }
    }

    void dotSum(Matrix right_mat, Matrix sum_mat)
    {
        if (right_mat.n_rows() != n_cols())
        {
            throw invalid_argument("right_mat rows must match the left matrix's columns");
        }

        if (sum_mat.n_rows() != n_rows() || sum_mat.n_cols() != right_mat.n_cols())
        {
            throw invalid_argument("sum_mat rows must match the left_mat's, and it's columns must match the right_mat's");
        }

        for (int i = 0; i < n_rows(); i++)
        {
            for (int j = 0; j < right_mat.n_cols(); j++)
            {
                for (int k = 0; k < n_cols(); k++)
                {
                    // iterate over left mat columns and right mat rows
                    sum_mat[i][j] += data(i,k) * right_mat[k][j];
                }
            }
        }
    }

    void dot(Matrix right_mat, Matrix result_mat)
    {
        if (right_mat.n_rows() != n_cols())
        {
            throw invalid_argument("right_mat rows must match the left matrix's columns");
        }

        if (result_mat.n_rows() != n_rows() || result_mat.n_cols() != right_mat.n_cols())
        {
            throw invalid_argument("result_mat rows must match the left_mat's, and it's columns must match the right_mat's");
        }

        //auto data_wrapper = (double (*)[row_spacing])data_ptr;

        for (int i = 0; i < n_rows(); i++)
        {
            for (int j = 0; j < right_mat.n_cols(); j++)
            {
                result_mat[i][j] = 0;
                for (int k = 0; k < n_cols(); k++)
                {
                    // iterate over left mat columns and right mat rows
                    result_mat[i][j] += data(i,k) * right_mat[k][j];
                }
            }
        }
    }

    Matrix dot(Matrix right_mat)
    {
        Matrix result_mat(n_rows(),right_mat.n_cols());
        dot(right_mat,result_mat);
        return result_mat;
    }
};

#endif // MATRIX_H
